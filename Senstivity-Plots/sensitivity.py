import csv
import numpy as np
import pylab
from lmfit.models import GaussianModel
from lmfit.models import LorentzianModel
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import math
from scipy.signal import savgol_filter


def axion_power(mass,k,C,B,V,Q,G):
    return (8.61*10**(-25)*(mass/30)*(k/0.5)*(C**2)*((B/9.0)**2)*(V/1)*(Q/(10**4))*((G/0.69)**2))

def noise_power(mass,T_sys,Q_a):
    return (6.0*10**(-19)*(mass/30)*(T_sys/6)*((10**6)/Q_a))

def g_AGamma(mass,C):
    return (2.0*10**(-16)*(mass)*C)

def C_AGamma(mass,k,SN,B,V,Q,G,T_eff,t):
    return (26.1309*((30/mass)**(1/4))*((SN/3)**(1/2))*(9.0/B)*((1/V)**(1/2))*(((10**4)/Q)**(1/2))*(0.69/G)*((T_eff/10)**(1/2))*((0.5/k)**(1/2))*((1/t)**(1/4)))

mass = 34.62
k = 0.493
SN = 5
B = 8.8
V = 0.224
Q = 1000
G = 0.53
T_sys= 350
t = 15*24 #measured time in hours


print ('C_AGamma is:', C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t))
C = C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t)
print ('the axion power is:', axion_power(mass,k,C,B,V,Q,G), 'Watt')
print('the noise power is:', noise_power(mass,T_sys,10**6), 'Watt')
print ('g_AGamma is:', g_AGamma(mass,C), 'GeV^-1')
Norm = g_AGamma(mass,2)


castlimit = 0.66e-10
QAUX_Limit = 1.03e-12
RADES2020_limit = g_AGamma(mass,C)
print (mass)
KSZV_limit = 2.0*10**(-16)*2*mass
Top_KSZV_limit = 2.0*10**(-16)*10*mass
Bottom_KSZV_limit = 2.0*10**(-16)*0.29*mass

def Lorentzian(w,w_0,Q):
	return 1/(1+((w/w_0-1)**2)*(4*Q**2))#1.0/(1.0 + 4.0*Q**2.0(w/w_0)-1.0)


center_test = 8.4e9
xtest = np.linspace(center_test-6e6,center_test+6e6,5000, endpoint = True)

k = 0.1
C = C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t)
RADES2020_limit_wc = g_AGamma(mass,C)

t = (200*90)/3600
C = C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t)
RADES2020_limit_now = g_AGamma(mass,C)

t = 30*24*12
C = C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t)
RADES2020_limit_now_2 = g_AGamma(mass,C)

k = 0.5
Q = 10000
T_sys= 10
t = 30*24*3
SN = 5

C = C_AGamma(mass,k,SN,B,V,Q,G,T_sys,t)
print (C)
RADES2020_limit_wish = g_AGamma(mass,C)

KSZV_limit_array = np.array([KSZV_limit,KSZV_limit,KSZV_limit])
bottom = KSZV_limit - Bottom_KSZV_limit
top = Top_KSZV_limit - KSZV_limit 
Top_KSZV_limit_array = np.array([Top_KSZV_limit,Top_KSZV_limit,Top_KSZV_limit])
Bottom_KSZV_limit_array = np.array([Bottom_KSZV_limit,Bottom_KSZV_limit,Bottom_KSZV_limit])

conversionfactor = (2.417989242E14)#**-1 Factor to convert from eV to GHz
mass = xtest*conversionfactor**-1

x_freq = np.array([8,8.385,9.3])*1e9
mass2 = x_freq*conversionfactor**-1
mass2_bottom = (center_test-12e6)*conversionfactor**-1
mass2_top = (center_test+12e6)*conversionfactor**-1


plt.figure(figsize=(10,7))
plt.plot(mass2*1e6, KSZV_limit_array,"black", label = "KSZV Model", linewidth = 2)
plt.fill_between(mass2*1e6, KSZV_limit_array-bottom, KSZV_limit_array + top, color = "yellow")
plt.plot(mass2*1e6, [castlimit,castlimit,castlimit], label = "CAST solar axion limit", linewidth = 2)
plt.fill_between(mass*1e6, RADES2020_limit_wish*(Lorentzian(xtest,8.4e9,10000))**-1, castlimit+castlimit*0.2,color = "red", label = "Improved setup 12 Months, coupling = 1,Q=10000,T=10K")
#plt.fill_between(xtest, RADES2020_limit*(Lorentzian(xtest,8.4e9,1000))**-1, castlimit+castlimit*0.2,color = "red", label = "RADES Limit 15days, coupling = 0.5,Q=1000,T=350K")
#plt.fill_between(mass*1e6, RADES2020_limit_now_2*(Lorentzian(xtest,8.4e9,1000))**-1, castlimit+castlimit*0.2,color = "magenta", label = "2020-setup 12 Months, coupling = 0.1,Q=1000,T=350K")
plt.fill_between(mass*1e6, RADES2020_limit_wc*(Lorentzian(xtest,8.4e9,1000))**-1, castlimit+castlimit*0.2,color = "green", label = "2020-setup 15days, coupling = 0.1,Q=1000,T=350K")
plt.title("RADES prospects")
#plt.xlabel("mass [$\mu$eV]")
#plt.ylabel("axion coupling [GeV$^{-1}$]")
plt.xlabel("mass [$\mu$eV]", fontsize = 18)
plt.ylabel("$|g_{a\gamma}|$ [GeV$^{-1}$]", fontsize = 18)
plt.yscale('log')
plt.xlim(mass2_bottom*1e6,mass2_top*1e6)
plt.legend()
plt.tick_params(axis='both', which='major', labelsize=18)
plt.tick_params(axis='both', which='minor', labelsize=18)
#plt.savefig('TestFigure.pdf', dpi=200)
plt.show()




