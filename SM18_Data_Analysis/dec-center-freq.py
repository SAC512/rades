###################################################################################################
# Author: Sergio Arguedas
#
#
# Purpose: Generates a file containing decimal data from a file with raw hexadecimal RADES data and a second file with the center frequency of the resonance peak 
#
# Usage in terminal: 
# 
#	>> python3 dec-center-freq
#
# Dependencies:
#	1) hex_to_dec_v2.py
#	2) RADES_lib.py
###################################################################################################
import numpy as np
import matplotlib.pyplot as plt
import math
import subprocess
import glob, os

###################################################################################################
#Variables added manually:
#TTI-DAQ resolution bandwith: BW 
#Local oscillator frequency: LO_freq
###################################################################################################

#user has to decide through which files the want the code to run.
#at the moment the files have to be in the same folder as the script
filename = "2021*.txt"

#Converts the hexadecimal files to decimal files
for file in sorted(glob.glob(filename), key=os.path.getmtime):
	print(file)
	c = file
	os.system("python3 hex_to_dec_v2.py %s %s" %(file,"dec_"+file))

#Use the decimal files to compute the center frequency of the resonance peak.
for file in sorted(glob.glob("dec_"+file), key=os.path.getmtime):  
	print(file)
	RU = np.genfromtxt(file, dtype=np.float64, delimiter = '\t', skip_header = 9, encoding=None)
	deletetrace = 0
	testtrace = 3
	BW = 4577.63671
	LO_freq = 0#8.240672e9 #GHz

	xfull = RU[0,1::]
	f_start = LO_freq+134.0011597e6
	x_freq = f_start + xfull*BW

	RawData=np.zeros((len(RU)-1-deletetrace,len(RU[0,:])-1)) #
	for i, column in enumerate (RU[0,:]):
		#print (i)
		if i == 0:
			pass
		else:
			for j in range(len(RU)-deletetrace):#
				if j == 0:
					pass
				else:
					b = (10**(RU[j,i]/10))	
					RawData[j-1][i-1] = b

	print ("the # of traces is: ", len(RawData))

	maxfreq_fit = np.array([])

	for i in range (len(RawData)):
		result = np.where(RawData[i,:] == np.amax(RawData[i,:]))
		#print (i, " = ", result[0])
		maxfreq = int(max(result[0]))
		lower = maxfreq - 25
		upper = maxfreq + 25

		mod = LorentzianModel ()

		x = x_freq[lower:upper]
		y = RawData[i,lower:upper]
		pars = mod.guess(y,x=x)
		out = mod.fit(y, pars, x=x)
		#print (out.fit_report(min_correl=0.25))
		sigma     = out.params['sigma'].value
		center    = out.params['center'].value
		amplitude = out.params['amplitude'].value
		fwhm      = out.params['fwhm'].value
		height    = out.params['height'].value
	
		maxfreq_fit = np.append(maxfreq_fit,center)
		#Uncomment these lines if you want to see the fit plotted to each spectrum
		#plt.plot(x_freq, RawData[i,:], label = "S12")
		#plt.plot(x, out.best_fit, label = "fit", color = "red")
		#plt.title("S-21-CAST-NoLNA with FWHM")
		#plt.xlabel("Frequency [Hz]")
		#plt.ylabel("Power [linear units]")
		#plt.legend()
		#plt.show()

	###Creates a txt file with 90s spectra 
	Outfile=open("fit-binmaxfreq-%s" %file,"w")
	np.savetxt(Outfile, maxfreq_fit)
	Outfile.close()

