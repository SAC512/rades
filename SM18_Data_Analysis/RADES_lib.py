###################################################################################################
# Author: Cristian Cogollos
#
# Last Update: 29th November 2019
#
# Purpose: Library containing general purpose classes and functions for RADES 
#		   data analysis and data preparation
#
# Dependencies: (library) numpy as np
#				(library) datetime from datetime
# 
###################################################################################################

from datetime import datetime
import numpy as np

###################################################################################################
#
#	Class: file_RADES_metadata
#
#	Author: Cristian Cogollos
#	Variables:
#	Functions:
#
#	Dependencies:
#
#
###################################################################################################
class file_RADES_metadata:
	def __init__(self,input_filename,min_freq=134001159.7,max_freq=145999145.5):
		self.min_freq=min_freq
		self.max_freq=max_freq
		with open(input_filename,"r") as infile:
			infile.readline()
			infile.readline()
			line=infile.readline()
			self.acq_FFTs=int(line[line.find(":")+2:-1])
			line= infile.readline()
			self.acq_time=float(line[line.find(":")+2:line.find("s")-2])
			line= infile.readline()
			self.bits=int(line[line.find(":")+2:-1])
			infile.readline()
			line= infile.readline()
			self.bandwidth_res=float(line[line.find(":")+2:line.find("H")-2])
			line= infile.readline()
			self.bandwidth_points=int(line[line.find(":")+2:-1])
			line= infile.readline()
			ini_time=line[line.find('t ')+2:-1]
			ini_time.replace('-','//')
			self.creation_dt=datetime.strptime(ini_time,'%m/%d/%Y %I:%M:%S %p')

	def print(self):
		print(self.min_freq)
		print(self.max_freq)
		print(self.acq_FFTs)
		print(self.acq_time)
		print(self.bits)
		print(self.bandwidth_res)
		print(self.bandwidth_points)
		print(self.creation_dt)

	def fprint(self, output_filename):
		with open(output_filename,"w") as outfile:
			outfile.write('==================================\n')
			outfile.write('Acc FFT: '+str(self.acq_FFTs)+'\n')
			outfile.write('Interval: '+str(self.acq_time) +' s\n')
			outfile.write('Bits: '+str(self.bits)+'\n')
			outfile.write('Signal bandwidth: '+str(self.min_freq/10**6)+' MHz - '+str(self.max_freq/10**6)+' MHz\n')
			outfile.write('Resolution bandwidth: '+str(self.bandwidth_res) +' Hz\n')
			outfile.write('Bandwidth points: '+str(self.bandwidth_points)+'\n')
			outfile.write('Created at '+(self.creation_dt).strftime("%Y-%m-%d %H:%M:%S")+'\n')
			outfile.write('==================================\n')

###################################################################################################
#
#	Class: columns_metadata
#
#	Author: Cristian Cogollos
#	Variables:
#	Functions:
#
#	Dependencies:
#
#
###################################################################################################
class columns_metadata:
	def __init__(self,inputfile_metadata):
		self.columns=[]
		#self.columns.append('Date')
		#self.columns.append('Time')
		self.columns.append('Timestamp')
		for i in range(1,inputfile_metadata.bandwidth_points+1):
			self.columns.append(str(i))
	def add_temp(self):
		self.columns.insert(3,'Temperature')
	def add_mag(self):
		self.columns.insert(3,'B_Field')
	def add_LO(self):
		self.columns.insert(3,'LO_freq')
	def fprint(self,output_filename):
		line='\t'.join(self.columns)
		outfile=open(output_filename,'a')
		outfile.write(line+'\n')

###################################################################################################
#
#	Class: RADES_measurement
#
#	Author: Cristian Cogollos
#	Description: Power spectrum and metadata for ONE measurement.
#	Variables:
#	Functions:
#
#	Dependencies: (library - imported outside class) numpy as np
#				  (library - imported outside class) matplotlib.pyplot as plt
#
#
###################################################################################################
class RADES_measurement:
	def __init__(self,input_filename,N_line):
		self.file=input_filename
		self.fileLine=N_line
		self.dt=datetime.min
		with open(input_filename,'r') as infile:
			for i in range(10+N_line):
				infile.readline()
			line=infile.readline()
			line=line.split("\t")
			self.dt.strptime(line[0]+" "+line[1],'%Y-%m-%d %H:%M:%S')
			line=line[3:-1]
			for j,val in enumerate(line):
				line[j]=float(val)
			self.spectrum=np.asarray(line)
			self.noisy_bins=[]
	def get_magnet(self,input_mag):
		self.mag=input_mag
	def get_LOfreq(self,input_LO):
		self.LOfreq=input_LOfreq
	def get_Temp(self,input_Temp):
		self.Temp=input_Temp
	def normalise_spectrum(self,mean_spectrum):
		norm_spectrum=self.spectrum/mean_spectrum
		return norm_spectrum
#	def SGfilter(self,)
#		self.SGspectrum=np.asarray([])
#		self.SGspectrum
	def get_noisy_bins(self,mean_spectrum,threshold):
		NormData=self.spectrum-mean_spectrum
		NormData=NormData/np.amax(NormData)		
	def plot(self):
		plt.plot(self.spectrum)
	def fprint(self,output_filename):
		with open(output_filename,'a') as outfile:
			outfile.write('\n'+(self.dt).strftime('%Y-%m-%d'))
			outfile.write('\t'+(self.dt).strftime('%H:%M:%S'))
			outfile.write('\t'+str(datetime.timestamp(self.dt)))
			outfile.write('\t'+str(self.mag))
			outfile.write('\t'+str(self.Temp))
			outfile.write('\t'+str(self.LOfreq))
			spectrum_char=np.char.mod('%.10f',self.spectrum)
			outfile.write('\t'+'\t'.join(spectrum_char))
###################################################################################################
#
#	Class: RADES_measurement_LO
#
#	Author: Cristian Cogollos
#	Description: Power spectrum and metadata for ONE measurement.
#	Variables:
#	Functions:
#
#	Dependencies: (library - imported outside class) numpy as np
#				  (library - imported outside class) matplotlib.pyplot as plt
#
#
###################################################################################################
class RADES_measurement_LO:
	def __init__(self,input_filename,N_line):
		with open(input_filename,'r') as infile:
			for i in range(10+N_line):
				infile.readline()
			line=infile.readline()
			if line == '':
				print('Empty line')
			else:
				self.file=input_filename
				self.fileLine=N_line
				self.dt=datetime.min
				line=line.split("\t")
				self.dt=datetime.strptime(line[0]+" "+line[1],'%Y-%m-%d %H:%M:%S')
				self.mag=float(line[3])
				self.Temp=float(line[4])
				self.LOfreq=float(line[5])
				line=line[6:-1]
				for j,val in enumerate(line):
					line[j]=float(val)
				self.spectrum=np.asarray(line)
				self.noisy_bins=[]
	def normalise_spectrum(self,mean_spectrum):
		norm_spectrum=self.spectrum/mean_spectrum
		return norm_spectrum
#	def SGfilter(self,)
#		self.SGspectrum=np.asarray([])
#		self.SGspectrum
	def get_noisy_bins(self,mean_spectrum,threshold):
		NormData=self.spectrum-mean_spectrum
		NormData=NormData/np.amax(NormData)		
	def plot(self):
		plt.plot(self.spectrum)
	def fprint(self,output_filename):
		with open(output_filename,'a') as outfile:
			outfile.write('\n'+(self.dt).strftime('%Y-%m-%d'))
			outfile.write('\t'+(self.dt).strftime('%H:%M:%S'))
			outfile.write('\t'+str(datetime.timestamp(self.dt)))
			outfile.write('\t'+str(self.mag))
			outfile.write('\t'+str(self.Temp))
			outfile.write('\t'+str(self.LOfreq))
			spectrum_char=np.char.mod('%.10f',self.spectrum)
			outfile.write('\t'+'\t'.join(spectrum_char))

###################################################################################################
#
#	Function: peak_displacement(reference, compared)
#
#	Author: Cristian Cogollos
#	Purpose: Compares how displaced is the maximum value for a spectrum with respect to a reference one. The output is given in bins.
#	Input:  (np.array) reference -> Reference spectrum.
#			(np.array) compared -> Spectrum we want to compare to the reference one.
#	Output: (int) distance -> Distance in bins between the maximum of the two spectra.
#
#	Dependencies: (library - imported outside function) numpy as np
#
#
###################################################################################################
def peak_displacement(reference, compared):
	if np.size(reference) == np.size(compared):  # Check similar size of input vectors
		distance=abs(np.argmax(reference)-np.argmax(compared)) # we compute the distance between the two peaks
	else:
		print('Error: The amount of points for the given spectra are different.')
	return distance

###################################################################################################
#
#	Function: mean_spectrum(input_filename)
#
#	Author: Cristian Cogollos
#	Purpose: Obtains the mean spectrum from a given DEC_file
#	Input:  (string) input_filename -> Path+Name to the input file
#	Output: (np.array) spectrum -> Mean spectrum for the input file
#
#	Dependencies: (library - imported outside function) numpy as np
#				  (library - imported outside function) datetime from datetime
#				  (library - imported outside function) timedelta from timedelta
#
#
###################################################################################################
def mean_spectrum(input_filename):
	spectrum=np.zeros(2622)
	N=0
	with open(input_filename,'r') as infile:
		for i in range(10):
			infile.readline()
		for line in infile:
			line=line.split("\t")
			spectrum=spectrum+np.asfarray(line[6:-1],float)
			N=N+1
		spectrum=spectrum/N
	return spectrum
