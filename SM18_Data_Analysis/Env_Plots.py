###################################################################################################
# Author: Sergio Arguedas
#
#
# Purpose: Make a plot showing the time evolution of the enviromental data  
#
# Usage in terminal: 
# 
#	>> python3 Env_Plots.py
#
###################################################################################################

import numpy as np
import matplotlib.pyplot as plt
import math
import subprocess
import glob, os
from lmfit.models import LorentzianModel
import datetime as dt
import matplotlib.dates as md

filepath = "/home/Combine-Data/11-03/env_dec_2021*.txt" 
#use the path where your env_dec files are located in your local pc or the path to the RADES cernbox.

fig, axs = plt.subplots(2, 2, figsize=(15,8))
fig.suptitle('3rd of November 2021') #Figure's title
for file in sorted(glob.glob(filepath)):
	print(file)
	################################################
	#Filter between Magnet on and Magnet off
	################################################
	RawData = np.genfromtxt(file, dtype=np.float64, delimiter = ' ', skip_header = 10, encoding=None)
	dates= [dt.datetime.fromtimestamp(RawData[i,0]) for i in range(len(RawData))]
	xfmt = md.DateFormatter('%H:%M')
	axs[0, 0].plot(dates, RawData[:,1],"b")
	axs[0, 0].set(xlabel='time', ylabel='Temperature[K]')
	axs[0, 0].xaxis.set_major_formatter(xfmt)
	axs[0, 1].plot(dates, RawData[:,2],"b")
	axs[0, 1].set(xlabel='time', ylabel='Pressure[bar]')
	axs[0, 1].xaxis.set_major_formatter(xfmt)
	axs[1, 0].plot(dates, RawData[:,3],"b")
	axs[1, 0].set(xlabel='time', ylabel='Current[A]')
	axs[1, 0].xaxis.set_major_formatter(xfmt)	
	axs[1, 1].plot(dates, RawData[:,4]/1e6,"b")
	axs[1, 1].set(xlabel='time', ylabel='IF_Center_Freq[MHz]')
	axs[1, 1].xaxis.set_major_formatter(xfmt)

fig.savefig('EnviromentalVariables-3rdNov.pdf', dpi=200)
plt.show()




