###################################################################################################
# Author: Sergio Arguedas
#
#
# Purpose: Combine the data on the timber files to the decimal data 
#
# Usage in terminal: 
# 
#	>> python3 dec-center-freq
#
# 	
###################################################################################################

import numpy as np
import matplotlib.pyplot as plt
import math
import subprocess
import glob, os
import pandas as pd
import xlrd
import openpyxl
from datetime import datetime

#at the moment the decimal files and timber have to be in the same folder as the script

for file in sorted(glob.glob("dec_2021*.txt")):
	print (file)
	file_name = file
	RawData = np.genfromtxt(file, dtype=np.float64, delimiter = '\t', skip_header = 9, encoding=None)
	Header = np.genfromtxt(file, dtype=None,delimiter = '\n', max_rows = 9, encoding=None)
	#print (Header[:,None])
	#Number of columns that are not frequency bins in the raw data
	N_env = 1
	################################################
	#Reads timestamp of the raw data and values of the center frequency
	################################################
	Raw_dat_timestap = np.genfromtxt(file, dtype=None, usecols = (0), delimiter = '\t', skip_header = 9, encoding=None) #Here it reads the first column of the file, which corresponds the time stamps. The first line is a string while the rest are floats. To read both types of data the dtype was set to None.
	CenterFreq = np.genfromtxt("fit-binmaxfreq-%s" %file, dtype=np.float64, encoding=None)

	##################################
	#Reads timber file
	#I had a hard time reading the Timber files, there is for sure a more effective way to read them and spare a few for loops in the programm. So feel free to upgrade this part of the code. 
	##################################
	for file in sorted(glob.glob("TIMBER*.xlsx")):
		print (file)		
		path = file 
		book = openpyxl.load_workbook(path)
		#print (book.sheetnames) #Uncomment this line to know the name of the excel sheets. Symbols like ":" in the excel sheet are changed when you open it with the previous line. E.g. RPTCA.SM18.RM.G:I_MEAS --> RPTCA.SM18.RM.G.I_MEAS
		Env_var = ["Temp","Pressure","B_field"] 
		
		for s, worksheets in enumerate(book.sheetnames):
			#print (Env_var[s])	
			sheet = book[worksheets]
			#start at row 0, length 2 row:
			sheet.delete_rows(0,2) #The second row of the timber files is empty and the pd.read_excel command thinks the file ends in the second row. Adding skiprow = 2 did not help.
			df2 = pd.read_excel(book, sheet_name=worksheets, engine="openpyxl") #engine="openpyxl" has to be used to read xlsx files. This will output a warning in the terminal. In principle it is not a problem, it just give you some information. See more about the warning in https://stackoverflow.com/questions/66214951/how-to-deal-with-warning-workbook-contains-no-default-style-apply-openpyxls 
			date = df2.iloc[:,0]
			Env_Tim = df2.iloc[:,1]
			Time_Tim = np.array([])	
			for j in range (len(date)):
				#Date is converted to unix time
				dummy_variable = int(datetime.timestamp(date[j]))
				Time_Tim = np.append(Time_Tim, dummy_variable)

			#function used to compare and find the nearest value of two objects.
			def find_nearest(array, value):
				    array = np.asarray(array)
				    idx = (np.abs(array - value)).argmin()
				    return array[idx]

			Env_Tim_val = np.array([])
			#checks if raw data was taken in the time interval of the timber file
			if max(RawData[1:,0]) < min(Time_Tim) or min(RawData[1:,0]) > max(Time_Tim):
				pass
			else:
				if s == 0:
					Env_var_array = np.zeros([len(RawData[1:]),len(Env_var)]) #an array is created where the enviromental data corresponding to the power spectra will be stored.
				else:
					pass
				for i in range (len(RawData[1:])):
					#Searches for the closest unix time value of the enviromental data given the unix time value of the power spectrum. 
					closest_val = find_nearest(Time_Tim, RawData[i+1,0])
					Time_Timlist = Time_Tim.tolist()
					index = (Time_Timlist.index(closest_val))
					Env_Tim_val = np.append(Env_Tim_val,Env_Tim[index])
				Env_var_array[:,s] = Env_Tim_val
	################################################
	#Adds columns with Temp, Pressure, B_field and the center frequency given by the fit
	################################################

	Output = np.concatenate((Raw_dat_timestap[1:,None],Env_var_array,CenterFreq[:,None], RawData[1:,N_env:]), axis = 1) #
	UpdatedEnv_dat = np.append(Raw_dat_timestap[0], Env_var)
	UpdatedEnv_dat = np.append(UpdatedEnv_dat, "Center_Frequency")
	Output1 = np.concatenate((UpdatedEnv_dat,RawData[0,N_env::]), axis = 0)
	FinalOutput = np.concatenate((Output1[None,:],Output))
	#print (FinalOutput, type(file))


	###Creates a txt file with the power spectra and the enviromental data
	Outfile=open("env_%s" %file_name,"w")
	np.savetxt(Outfile, Header[:,None], fmt="%s")
	np.savetxt(Outfile, FinalOutput, fmt="%s")
	Outfile.close()

#The final output hast the following structure:
#line 0:
#column 0 =  time stamp
#column 1 =  temperature
#column 2 =  pressure
#column 3 =  center frequency
#Rest of the columns = bin #

#Rest of the lines: Measured values for each spectrum.


