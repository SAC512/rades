###################################################################################################
# Author: Cristian Cogollos
#
# Last Update: 27th November 2019
#
# Purpose: Generates a file containing decimal data from a file with raw hexadecimal RADES data 
#
# Usage in terminal: 
# 
#	>> python hex_to_dec_v5.py <path_and_inputfilename> <output_file>
#
###################################################################################################


###################################################################################################
#
#	Function: convert_file(input_filename, output_filename, inputfile_metadata )
#
#	Author: Cristian Cogollos
#	Purpose: Writes the body of a RADES_decfile from a given TTI_hexfile
#	Input:  (string) input_filename -> Path+Name to the input file
#			(string) output_filename -> Path+Name to the output file
#			(header) input_header -> 
#	Dependencies: (library - imported outside) numpy as np
#				  (library - imported outside) datetime from datetime
#				  (library - imported outside) timedelta from timedelta
#
#
###################################################################################################
def convert_file(input_filename, output_filename, inputfile_metadata):
	meas_dt=inputfile_metadata.creation_dt
	with open(input_filename,"r") as infile:
		with open(output_filename,"a") as outfile:
			# We skip the header
			for i in range(11):
				infile.readline()
			for j,line in enumerate(infile):
				# Writing the date, time and timestamp
				meas_dt=meas_dt+timedelta(0,inputfile_metadata.acq_time)
				date=meas_dt.strftime("%Y-%m-%d")
				time=meas_dt.strftime("%H:%M:%S")
				timestamp=int(datetime.timestamp(meas_dt))
				#outfile.write(date+'\t'+time+'\t'+str(timestamp))
				outfile.write(str(timestamp))
				# We get the line with the values
				line=line[line.find(":")+2:-1]
				# Define a value with the line read
				hex_vec=line.split(",")
				# We convert the hex values to decimal applying TTI formula
				for hex_val in hex_vec:
					outfile.write('\t'+str((-131 + 10*np.log10(int(hex_val,16)))))
				outfile.write('\n')
				if (j+1)%100==0:
					print(str(j)+' spectra processed.')

#	MAIN PROGRAM BODY 	#

#	Imports Header
import numpy as np
import sys
from datetime import datetime
from datetime import timedelta
from RADES_lib import *

# Input and output file are given as input when the script is called
hexfile=sys.argv[1]
decfile=sys.argv[2]

#print('Converting the hexadecimal file: '+hexfile+' to decimal.')

decfile_metadata=file_RADES_metadata(hexfile)
decfile_metadata.fprint(decfile)

decfile_columns=columns_metadata(decfile_metadata)
decfile_columns.fprint(decfile)

convert_file(hexfile,decfile,decfile_metadata)
