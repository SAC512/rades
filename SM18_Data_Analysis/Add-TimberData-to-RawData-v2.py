import numpy as np
import matplotlib.pyplot as plt
import math
import subprocess
import glob, os
import pandas as pd
import xlrd
import openpyxl
from datetime import datetime

##################################
#Read timber files
##################################
Tim_pressure = np.genfromtxt("TIMBER_data_pressure.csv", dtype=np.float64, delimiter = ',', skip_header = 3, encoding=None)
Tim_temp = np.genfromtxt("TIMBER_data_temp.csv", dtype=np.float64, delimiter = ',', skip_header = 3, encoding=None)
Tim_current = np.genfromtxt("TIMBER_data_current.csv", dtype=np.float64, delimiter = ',', skip_header = 3, encoding=None)

##################################
#Read and combine the TTI data with the Timber files
##################################
for file in sorted(glob.glob("dec_2021*.txt")):
	print (file)
	RawData = np.genfromtxt(file, dtype=np.float64, delimiter = '\t', skip_header = 9, encoding=None)
	Header = np.genfromtxt(file, dtype=None,delimiter = '\n', max_rows = 9, encoding=None)
	#print (Header[:,None])
	N_env = 1 #Number of columns that are not frequency bins in the raw data
	TTI_timestamp= RawData[1:,0]

	CenterFreq = np.genfromtxt("fit-binmaxfreq-%s" %file, dtype=np.float64, encoding=None)

	#function used to compare and find the nearest value of two objects.
	def find_nearest(array, value):
		array = np.asarray(array)
		idx = (np.abs(array - value)).argmin()
		return array[idx]

	#arrays that are going to be filled with pressure, temp and current that correspond to the power spectra of the TTI-files
	pressure = np.array([])
	temp = np.array([])
	current = np.array([])

	for i in range (len(RawData[1:])):
	#Searches for the closest unix time value of the enviromental data given the unix time value of the power spectrum. 
		#print(i)
		closest_val_pressure = find_nearest(Tim_pressure[:,0]/1e6, RawData[i+1,0]) #The timber file unix time has to be divided by 1e6
		closest_val_temp = find_nearest(Tim_temp[:,0]/1e6, RawData[i+1,0])
		closest_val_current = find_nearest(Tim_current[:,0]/1e6, RawData[i+1,0])
		Time_Timlist_pressure = (Tim_pressure[:,0]/1e6).tolist()
		Time_Timlist_temp = (Tim_temp[:,0]/1e6).tolist()
		Time_Timlist_current = (Tim_current[:,0]/1e6).tolist()
		index_pressure = (Time_Timlist_pressure.index(closest_val_pressure))
		index_temp = (Time_Timlist_temp.index(closest_val_temp))
		index_current = (Time_Timlist_current.index(closest_val_current))
		pressure = np.append(pressure,Tim_pressure[index_pressure,1])
		temp = np.append(temp,Tim_temp[index_temp,1])
		current = np.append(current,Tim_current[index_current,1])

	################################################
	#Adds columns with Temp, Pressure, B_field and the center frequency given by the fit
	################################################
	Env_var = ["Timestamp","Temp","Pressure","Current","Center_frequency"]
	Output = np.concatenate((TTI_timestamp[:,None],temp[:,None],pressure[:,None],current[:,None],CenterFreq[:,None], RawData[1:,N_env:]), axis = 1) #
	Output1 = np.concatenate((Env_var,RawData[0,N_env::]), axis = 0)
	FinalOutput = np.concatenate((Output1[None,:],Output))
	#print (FinalOutput), type(file))
	
	###Creates a txt file with the power spectra and the enviromental data
	Outfile=open("env_%s" %file,"w")
	np.savetxt(Outfile, Header[:,None], fmt="%s")
	np.savetxt(Outfile, FinalOutput, fmt="%s")
	Outfile.close()

#The final output hast the following structure:
#line 0:
#column 0 =  time stamp
#column 1 =  temperature
#column 2 =  pressure
#column 3 =  center frequency
#Rest of the columns = bin #

#Rest of the lines: Measured values for each spectrum.
